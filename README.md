package daspro.recruitment;


import java.util.Scanner;

public class No3 {

    public static void main(String[] args) {
        Scanner br = new Scanner(System.in);

        String[] abjad = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
            "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
            "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
            ",", ".", "?", ""};

        String[] kodeMorse = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..",
            ".---", "-.-", ".-..", "--", "-.", "---", ".---.", "--.-", ".-.",
            "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", ".----",
            "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.",
            "-----", "--..--", ".-.-.-", "..--..", "/"};

        System.out.println("Masukkan Kode: ");
        String input = br.next();

        for (int i = 0; i < input.length(); i++) {

            if (input.equals(kodeMorse[i])) {
                System.out.println(abjad[i]);

            }

        }

    }
}
